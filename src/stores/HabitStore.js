import { writable } from "svelte/store";

const HabitStore = writable([
  {
    habit: "test",
    yes: 0,
    no: 0,
  },
]);

export default HabitStore;
